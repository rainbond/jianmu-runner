#!/bin/sh

cd ${JIANMU_WORKSPACE} && pwd

if [[ ! -z "${JIANMU_DOCKER_USERNAME}" ]]; then 
    docker login -u ${JIANMU_DOCKER_USERNAME} -p ${JIANMU_DOCKER_PASSWORD} ${JIANMU_REGISTRY_ADDRESS}; 
fi

set -x 
docker_build_arg=""
no_cache=""
if [[ ! -z "${JIANMU_BUILD_ARGS}" ]]; then
    build_args=$(echo ${JIANMU_BUILD_ARGS} | tr ',' ' ')
    for arg in ${build_args}
    do
      docker_build_arg="${docker_build_arg} --build-arg ${arg}"
    done
fi

if [[ "${JIANMU_NO_CACHE}" == "true" ]]
then
    no_cache="--no-cache"
fi

docker build ${no_cache} -f ${JIANMU_DOCKER_FILE} -t ${JIANMU_IMAGE_NAME}:${JIANMU_IMAGE_TAG} ${docker_build_arg} ${JIANMU_DOCKER_BUILD_PATH} && docker push ${JIANMU_IMAGE_NAME}:${JIANMU_IMAGE_TAG}    
if [[ "${JIANMU_IMAGE_CLEAN}" == true"" ]]; then
    docker rmi ${JIANMU_IMAGE_NAME}:${JIANMU_IMAGE_TAG} || true
fi

echo "{\"image_name\": \"${JIANMU_IMAGE_NAME}\",\"image_tag\": \"${JIANMU_IMAGE_TAG}\"}" > /tmp/result.json
#!/bin/bash

## Debug
# JIANMU_RAINBOND_URL=
# JIANMU_RAINBOND_TOKEN=
# JIANMU_TEAM_ID=
# JIANMU_REGION_ID=
# JIANMU_APPS_ID=
# JIANMU_COMPONENT_NAME=
# JIANMU_RBD_IMAGE=

# create component
function create_component () {
  create_component_result=$(curl --location --request POST "${JIANMU_RAINBOND_URL}/openapi/v1/teams/${JIANMU_TEAM_ID}/regions/${JIANMU_REGION_ID}/apps/${JIANMU_APPS_ID}/services" \
  --header "Authorization: ${JIANMU_RAINBOND_TOKEN}" \
  --header 'Content-Type: application/json' \
  --data-raw "{
    \"group_id\": ${JIANMU_APPS_ID},
    \"image\": \"${JIANMU_RBD_IMAGE}\",
    \"service_cname\": \"${JIANMU_COMPONENT_NAME}\",
    \"k8s_component_name\": \"${JIANMU_COMPONENT_NAME}\", 
    \"user_name\": \"${JIANMU_RBD_IMAGE_USERNAME}\",
    \"password\": \"${JIANMU_RBD_IMAGE_PASSWORD}\",
    \"is_deploy\": true,
    \"docker_cmd\": \"\"
  }")

  create_component_result_format=$(echo "$create_component_result" | jq .)
  create_component_result_code=$(echo "$create_component_result" | jq -r .code)
  create_component_result_msg=$(echo "$create_component_result" | jq -r .msg)


  if [ "$create_component_result_code" = "200" ]; then
    echo "INFO: create component success."
  elif [ "$create_component_result_msg" = "k8s component name exists" ]; then
    echo "WARN: Component already created."
    get_component
  else
    echo "ERROR: Create component failed, msg: ${create_component_result_format}."
    exit 1
  fi
}


# get component id
function get_component (){
  get_component_result=$(curl --location --request GET "${JIANMU_RAINBOND_URL}/openapi/v1/teams/${JIANMU_TEAM_ID}/regions/${JIANMU_REGION_ID}/apps/${JIANMU_APPS_ID}/services" \
  --header "Authorization: ${JIANMU_RAINBOND_TOKEN}")

  # get_component_result_format=$(echo "$get_component_result" | jq .)
  # get_component_result_code=$(echo "$get_component_result" | jq -r .k8s_component_name)
  get_component_result_length=$(echo "$get_component_result" | jq '.|length')
  for((i=0;i<get_component_result_length;i++)); do
    get_k8s_component_name=$(echo "$get_component_result" | jq -r ".[$i].k8s_component_name")
    if [ "$get_k8s_component_name" = "$JIANMU_COMPONENT_NAME" ]; then
      get_component_id=$(echo "$get_component_result" | jq -r ".[$i].service_alias")
      echo "INFO: get component id success, component id is ${get_component_id}."
      build_component "$get_component_id"
      break
    else
      echo "${RED}ERROR: get component id failed, Maybe the component is not deployed.${NC}"
      exit 1
    fi
  done
  
  
}

function build_component (){
  
  local USERNAME=${JIANMU_RBD_IMAGE_USERNAME:-null}
  local PASSWORD=${JIANMU_RBD_IMAGE_PASSWORD:-null}

  build_component_result=$(curl --location --request POST "${JIANMU_RAINBOND_URL}/openapi/v1/teams/${JIANMU_TEAM_ID}/regions/${JIANMU_REGION_ID}/apps/${JIANMU_APPS_ID}/services/$1/build" \
  --header "Authorization: ${JIANMU_RAINBOND_TOKEN}" \
  --header 'Content-Type: application/json' \
  --data-raw "{
      \"build_type\": \"docker_image\",
      \"repo_url\": \"${JIANMU_RBD_IMAGE}\",
      \"username\": \"${USERNAME}\",
      \"password\": \"${PASSWORD}\"
  }")

  build_component_result_format=$(echo "$build_component_result" | jq -r .event_id)

  if [ "$build_component_result_format" != "null" ]; then
    echo "INFO: build component success."
  else
    echo "ERROR: build component failed, msg: ${build_component_result_format}."
    exit 1
  fi
}


create_component